﻿namespace MADNSimulator
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schließenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datenabankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anzeigenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.offnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datenbankortÖffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hilfeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.wurfButton = new System.Windows.Forms.Button();
            this.wurfCheck = new System.Windows.Forms.CheckBox();
            this.listBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.red0 = new System.Windows.Forms.Label();
            this.red1 = new System.Windows.Forms.Label();
            this.red3 = new System.Windows.Forms.Label();
            this.red2 = new System.Windows.Forms.Label();
            this.blue0 = new System.Windows.Forms.Label();
            this.blue2 = new System.Windows.Forms.Label();
            this.blue3 = new System.Windows.Forms.Label();
            this.blue1 = new System.Windows.Forms.Label();
            this.green0 = new System.Windows.Forms.Label();
            this.green3 = new System.Windows.Forms.Label();
            this.green2 = new System.Windows.Forms.Label();
            this.green1 = new System.Windows.Forms.Label();
            this.yellow0 = new System.Windows.Forms.Label();
            this.yellow3 = new System.Windows.Forms.Label();
            this.yellow2 = new System.Windows.Forms.Label();
            this.yellow1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 533);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(861, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(38, 17);
            this.statusLabel.Text = "status";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.datenabankToolStripMenuItem,
            this.hilfeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(861, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.schließenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // schließenToolStripMenuItem
            // 
            this.schließenToolStripMenuItem.Name = "schließenToolStripMenuItem";
            this.schließenToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.schließenToolStripMenuItem.Text = "Schließen";
            // 
            // datenabankToolStripMenuItem
            // 
            this.datenabankToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.anzeigenToolStripMenuItem,
            this.offnenToolStripMenuItem,
            this.datenbankortÖffnenToolStripMenuItem});
            this.datenabankToolStripMenuItem.Name = "datenabankToolStripMenuItem";
            this.datenabankToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.datenabankToolStripMenuItem.Text = "Datenabank";
            // 
            // anzeigenToolStripMenuItem
            // 
            this.anzeigenToolStripMenuItem.Name = "anzeigenToolStripMenuItem";
            this.anzeigenToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.anzeigenToolStripMenuItem.Text = "Anzeigen";
            // 
            // offnenToolStripMenuItem
            // 
            this.offnenToolStripMenuItem.Name = "offnenToolStripMenuItem";
            this.offnenToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.offnenToolStripMenuItem.Text = "Offnen...";
            // 
            // datenbankortÖffnenToolStripMenuItem
            // 
            this.datenbankortÖffnenToolStripMenuItem.Name = "datenbankortÖffnenToolStripMenuItem";
            this.datenbankortÖffnenToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.datenbankortÖffnenToolStripMenuItem.Text = "Datenbankort öffnen";
            // 
            // hilfeToolStripMenuItem
            // 
            this.hilfeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoToolStripMenuItem});
            this.hilfeToolStripMenuItem.Name = "hilfeToolStripMenuItem";
            this.hilfeToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.hilfeToolStripMenuItem.Text = "Hilfe";
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(95, 22);
            this.infoToolStripMenuItem.Text = "Info";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 500);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // wurfButton
            // 
            this.wurfButton.Location = new System.Drawing.Point(519, 28);
            this.wurfButton.Name = "wurfButton";
            this.wurfButton.Size = new System.Drawing.Size(75, 23);
            this.wurfButton.TabIndex = 3;
            this.wurfButton.Text = "Würfeln";
            this.wurfButton.UseVisualStyleBackColor = true;
            // 
            // wurfCheck
            // 
            this.wurfCheck.AutoSize = true;
            this.wurfCheck.Location = new System.Drawing.Point(600, 32);
            this.wurfCheck.Name = "wurfCheck";
            this.wurfCheck.Size = new System.Drawing.Size(87, 17);
            this.wurfCheck.TabIndex = 4;
            this.wurfCheck.Text = "auto Würfeln";
            this.wurfCheck.UseVisualStyleBackColor = true;
            // 
            // listBox
            // 
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(519, 58);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(168, 472);
            this.listBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Orange;
            this.label1.Location = new System.Drawing.Point(693, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Würfel Gelb";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(693, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Würfel Rot";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(693, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Würfel Blau";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(693, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Würfel Grün";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(774, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(774, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "label6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(774, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(774, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "label8";
            // 
            // red0
            // 
            this.red0.AutoSize = true;
            this.red0.BackColor = System.Drawing.Color.Transparent;
            this.red0.Location = new System.Drawing.Point(31, 49);
            this.red0.Name = "red0";
            this.red0.Size = new System.Drawing.Size(15, 13);
            this.red0.TabIndex = 15;
            this.red0.Text = "R";
            // 
            // red1
            // 
            this.red1.AutoSize = true;
            this.red1.BackColor = System.Drawing.Color.Transparent;
            this.red1.Location = new System.Drawing.Point(78, 49);
            this.red1.Name = "red1";
            this.red1.Size = new System.Drawing.Size(15, 13);
            this.red1.TabIndex = 16;
            this.red1.Text = "R";
            // 
            // red3
            // 
            this.red3.AutoSize = true;
            this.red3.BackColor = System.Drawing.Color.Transparent;
            this.red3.Location = new System.Drawing.Point(78, 93);
            this.red3.Name = "red3";
            this.red3.Size = new System.Drawing.Size(15, 13);
            this.red3.TabIndex = 17;
            this.red3.Text = "R";
            // 
            // red2
            // 
            this.red2.AutoSize = true;
            this.red2.BackColor = System.Drawing.Color.Transparent;
            this.red2.Location = new System.Drawing.Point(31, 93);
            this.red2.Name = "red2";
            this.red2.Size = new System.Drawing.Size(15, 13);
            this.red2.TabIndex = 18;
            this.red2.Text = "R";
            // 
            // blue0
            // 
            this.blue0.AutoSize = true;
            this.blue0.BackColor = System.Drawing.Color.Transparent;
            this.blue0.Location = new System.Drawing.Point(420, 49);
            this.blue0.Name = "blue0";
            this.blue0.Size = new System.Drawing.Size(14, 13);
            this.blue0.TabIndex = 19;
            this.blue0.Text = "B";
            // 
            // blue2
            // 
            this.blue2.AutoSize = true;
            this.blue2.BackColor = System.Drawing.Color.Transparent;
            this.blue2.Location = new System.Drawing.Point(420, 93);
            this.blue2.Name = "blue2";
            this.blue2.Size = new System.Drawing.Size(14, 13);
            this.blue2.TabIndex = 20;
            this.blue2.Text = "B";
            // 
            // blue3
            // 
            this.blue3.AutoSize = true;
            this.blue3.BackColor = System.Drawing.Color.Transparent;
            this.blue3.Location = new System.Drawing.Point(467, 93);
            this.blue3.Name = "blue3";
            this.blue3.Size = new System.Drawing.Size(14, 13);
            this.blue3.TabIndex = 21;
            this.blue3.Text = "B";
            // 
            // blue1
            // 
            this.blue1.AutoSize = true;
            this.blue1.BackColor = System.Drawing.Color.Transparent;
            this.blue1.Location = new System.Drawing.Point(467, 49);
            this.blue1.Name = "blue1";
            this.blue1.Size = new System.Drawing.Size(14, 13);
            this.blue1.TabIndex = 22;
            this.blue1.Text = "B";
            // 
            // green0
            // 
            this.green0.AutoSize = true;
            this.green0.Location = new System.Drawing.Point(420, 441);
            this.green0.Name = "green0";
            this.green0.Size = new System.Drawing.Size(18, 13);
            this.green0.TabIndex = 23;
            this.green0.Text = "Gr";
            // 
            // green3
            // 
            this.green3.AutoSize = true;
            this.green3.Location = new System.Drawing.Point(463, 487);
            this.green3.Name = "green3";
            this.green3.Size = new System.Drawing.Size(18, 13);
            this.green3.TabIndex = 24;
            this.green3.Text = "Gr";
            // 
            // green2
            // 
            this.green2.AutoSize = true;
            this.green2.Location = new System.Drawing.Point(420, 487);
            this.green2.Name = "green2";
            this.green2.Size = new System.Drawing.Size(18, 13);
            this.green2.TabIndex = 25;
            this.green2.Text = "Gr";
            // 
            // green1
            // 
            this.green1.AutoSize = true;
            this.green1.Location = new System.Drawing.Point(463, 441);
            this.green1.Name = "green1";
            this.green1.Size = new System.Drawing.Size(18, 13);
            this.green1.TabIndex = 26;
            this.green1.Text = "Gr";
            // 
            // yellow0
            // 
            this.yellow0.AutoSize = true;
            this.yellow0.Location = new System.Drawing.Point(31, 441);
            this.yellow0.Name = "yellow0";
            this.yellow0.Size = new System.Drawing.Size(21, 13);
            this.yellow0.TabIndex = 27;
            this.yellow0.Text = "Ge";
            // 
            // yellow3
            // 
            this.yellow3.AutoSize = true;
            this.yellow3.Location = new System.Drawing.Point(72, 487);
            this.yellow3.Name = "yellow3";
            this.yellow3.Size = new System.Drawing.Size(21, 13);
            this.yellow3.TabIndex = 28;
            this.yellow3.Text = "Ge";
            // 
            // yellow2
            // 
            this.yellow2.AutoSize = true;
            this.yellow2.Location = new System.Drawing.Point(31, 487);
            this.yellow2.Name = "yellow2";
            this.yellow2.Size = new System.Drawing.Size(21, 13);
            this.yellow2.TabIndex = 29;
            this.yellow2.Text = "Ge";
            // 
            // yellow1
            // 
            this.yellow1.AutoSize = true;
            this.yellow1.Location = new System.Drawing.Point(72, 441);
            this.yellow1.Name = "yellow1";
            this.yellow1.Size = new System.Drawing.Size(21, 13);
            this.yellow1.TabIndex = 30;
            this.yellow1.Text = "Ge";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(204, 483);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Ge";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 555);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.yellow1);
            this.Controls.Add(this.yellow2);
            this.Controls.Add(this.yellow3);
            this.Controls.Add(this.yellow0);
            this.Controls.Add(this.green1);
            this.Controls.Add(this.green2);
            this.Controls.Add(this.green3);
            this.Controls.Add(this.green0);
            this.Controls.Add(this.blue1);
            this.Controls.Add(this.blue3);
            this.Controls.Add(this.blue2);
            this.Controls.Add(this.blue0);
            this.Controls.Add(this.red2);
            this.Controls.Add(this.red3);
            this.Controls.Add(this.red1);
            this.Controls.Add(this.red0);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.wurfCheck);
            this.Controls.Add(this.wurfButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem schließenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datenabankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anzeigenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem offnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datenbankortÖffnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hilfeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button wurfButton;
        private System.Windows.Forms.CheckBox wurfCheck;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label red0;
        private System.Windows.Forms.Label red1;
        private System.Windows.Forms.Label red3;
        private System.Windows.Forms.Label red2;
        private System.Windows.Forms.Label blue0;
        private System.Windows.Forms.Label blue2;
        private System.Windows.Forms.Label blue3;
        private System.Windows.Forms.Label blue1;
        private System.Windows.Forms.Label green0;
        private System.Windows.Forms.Label green3;
        private System.Windows.Forms.Label green2;
        private System.Windows.Forms.Label green1;
        private System.Windows.Forms.Label yellow0;
        private System.Windows.Forms.Label yellow3;
        private System.Windows.Forms.Label yellow2;
        private System.Windows.Forms.Label yellow1;
        private System.Windows.Forms.Label label9;
    }
}

