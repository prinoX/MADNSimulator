﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MADNSimulator
{
    class Game
    {
        Player[] players;
        int turn;

        const int RED = 0;
        const int BLUE = 1;
        const int GREEN = 2;
        const int YELLOW = 3;

        public Game()
        {
            turn = 0;
        }

        public void doTurn()
        {
            int dice = throwDice();

        }

        private int throwDice()
        {

            Random rand = new Random();

            int diceShows = rand.Next(1, 7);

            return diceShows;

        }


    }
}
